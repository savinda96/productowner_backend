﻿require('rootpath')();
const express = require('express');
const path =require('path');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
const jwt = require('_helpers/jwt');
const router = express.Router();
const errorHandler = require('_helpers/error-handler');
const userService = require('./users/user.service');




app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
//app.use(jwt());

// api routes
app.use('/users', require('./users/users.controller'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});

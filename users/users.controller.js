﻿const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.json());
const userService = require('./user.service');

// routes

router.post('/tauthenticate', tauthenticate);
router.post('/tregister', tregister);
router.get('/treatment', getAll);
router.get('/getu', getAllusers);
router.patch('/approve/:id',approve);
router.patch('/uapprove/:id',uapprove);
router.patch('/reset/',reset);




module.exports = router;


function tauthenticate(req, res, next) {
    userService.tauthenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}



function tregister(req, res, next) {
    console.log(req.body);
    userService.tcreate(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}



function approve(req, res, next) {
    console.log(req.params.id);
    console.log(req.body);
    userService.approve(req.params.id,req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function uapprove(req, res, next) {
    console.log(req.params.id);
    console.log(req.body);
    userService.uapprove(req.params.id,req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function reset(req, res, next) {
    console.log(req.body);
    userService.reset(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(injurys => res.json(injurys))
        .catch(err => next(err));
}

function getAllusers(req, res, next) {
    userService.getAllusers()
        .then( users=> res.json(users))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}


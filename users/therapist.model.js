const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    fname: { type: String, required: true },
    lname: { type: String, required: true },
    gender: { type: String, required: true },
    contactNo:{type:String, required:true},
    patients:{type:[Object],required:false},
    approved:{type:String,default:false,required:true},
    createdDate: { type: Date, default: Date.now }
},{collection:'p_owners'});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('p_owners', schema);
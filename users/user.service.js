﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');

const Injury=db.Injury;
const Therapist=db.Therapist;


const reqired={ RequiredProgress: [ { Movements: 0, Time: 0 } ],
PatientID: '5d9e25e36295435a8431078d',
TreatmentID: '5d614e2f2c3ad303a3dac1fd',
StartDate: '2019-11-24T18:30:00.000Z',
EndDate: '2019-11-29T18:30:00.000Z',
};

module.exports = {
    update,
    getAll,
    tcreate,
    approve,
    uapprove,
    getAllusers,
    tauthenticate,
    reset
    
};


async function tauthenticate({ username, password }) {
    const user = await Therapist.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}




 async function approve(id,userParam) {
    const injury = await Injury.findById(id);
    Object.assign(injury, userParam);

    await injury.save();

}

async function uapprove(id,userParam) {
    const therap = await Therapist.findById(id);
    Object.assign(therap, userParam);

    await therap.save();

}

async function reset(userParam) {
    await Injury.updateMany({},{$set:{paid:false}});

}


async function getAll() {
    return await Injury.find();
}

async function getAllusers() {
    return await Therapist.find();
}


async function tcreate(userParam) {
    // validate
    if (await Therapist.findOne({ username: userParam.username })) {
        throw 'Email "' + userParam.username + '" is already in use';
    }    

    const user = new Therapist(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

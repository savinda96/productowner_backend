const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    s_name: { type: String, required: true },
    o_name: { type: String, required: true },
    s_id: { type: String, required: true },
    paid:{type:Boolean,required:true}
},{collection:'cshops'});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('cshops', schema);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    TreatmentID: { type: Schema.Types.ObjectId, required: true },
    vidUrl: { type: String, required: true },
    imgUrl: { type: String, required: true }
},{collection:'exvideos'});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Videos', schema);